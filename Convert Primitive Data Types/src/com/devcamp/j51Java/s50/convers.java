package com.devcamp.j51Java.s50;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import javax.transaction.xa.XAException;

public class convers {
    public static void arrayToArrayList() {
        String[] strings = new String[] {"a", "b", "c", "h"};
        ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(strings)) ;
        System.out.println(arrayList);
    }

    public static void arrayToArrayList1() {
        Integer[] integers = {2, 4, 5, 7, 9, 12, 123} ;
        ArrayList<Integer> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, integers);
        System.out.println(arrayList);
    }

    public static void arrayListToArray() {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(2);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(8);
        arrayList.add(0);
        arrayList.add(12);
        arrayList.add(31);
        arrayList.add(52);
        arrayList.add(16);
        Integer[] array = new Integer[arrayList.size()];
        for(int i = 0 ; i < arrayList.size() ; i++){
            array[i] = arrayList.get(i) ;
        }
    }

    public static void main(String[] args) {
        convers.arrayToArrayList();
        convers.arrayToArrayList1();
        convers.arrayListToArray();
    }
}
